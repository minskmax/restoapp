﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestoApi.DataAccess.Migrations
{
    public partial class NewsColors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AccentColor",
                table: "News",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BackColor",
                table: "News",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccentColor",
                table: "News");

            migrationBuilder.DropColumn(
                name: "BackColor",
                table: "News");
        }
    }
}
