﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestoApi.DataAccess.Migrations
{
    public partial class firebaseindex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Token",
                table: "FirebaseTokens",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FirebaseTokens_Token",
                table: "FirebaseTokens",
                column: "Token",
                unique: true,
                filter: "[Token] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FirebaseTokens_Token",
                table: "FirebaseTokens");

            migrationBuilder.AlterColumn<string>(
                name: "Token",
                table: "FirebaseTokens",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
