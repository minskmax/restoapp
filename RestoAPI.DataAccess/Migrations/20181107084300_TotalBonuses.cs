﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestoApi.DataAccess.Migrations
{
    public partial class TotalBonuses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TotalBonusesCount",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalBonusesCount",
                table: "AspNetUsers");
        }
    }
}
