﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RestoApi.DataAccess.Common.Models;
using RestoAPI.DataAccess.Common.Models;

namespace RestoApi.DataAccess.Context
{
    public class RestoContext : IdentityDbContext<UserDbModel, RoleDbModel, string>
    {
        public RestoContext(DbContextOptions<RestoContext> options) : base(options)
        { }

        public DbSet<NewsDbModel> News { get; set; }
        public DbSet<ImageDbModel> Images { get; set; }
        public DbSet<LevelDbModel> Levels { get; set; }
        public DbSet<BonusTransactionDbModel> Bonuses { get; set; }
        public DbSet<UserProfileDbModel> Profiles { get; set; }
        public DbSet<FirebaseTokenDbModel> FirebaseTokens { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<UserDbModel>()
                .HasOne(x => x.Level)
                .WithMany()
                .HasForeignKey(x => x.LevelId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<BonusTransactionDbModel>()
                .HasOne(x => x.User)
                .WithMany(y => y.BonusTransactions)
                .HasForeignKey(y => y.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ImageDbModel>()
                .HasOne(x => x.News)
                .WithMany(y => y.Images)
                .HasForeignKey(y => y.NewsId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<UserProfileDbModel>()
                .HasOne(x => x.User)
                .WithOne(x => x.Profile)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<FirebaseTokenDbModel>()
                .HasOne(x => x.User)
                .WithOne(x => x.FirebaseToken)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<FirebaseTokenDbModel>()
                .HasIndex(x => x.Token)
                .IsUnique();

            base.OnModelCreating(modelBuilder);
        }
    }
}
