﻿using RestoAPI.DataAccess.Common.Models.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace RestoApi.DataAccess.Common.Models
{

    [DebuggerDisplay("Id = {Id}, News Id={NewsId}")]
    public class ImageDbModel : IntEntity
    {
        public int NewsId { get; set; }
        public byte[] Thumbnail { get; set; }
        public byte[] Image { get; set; }
        public virtual NewsDbModel News { get; set; }
    }
}
