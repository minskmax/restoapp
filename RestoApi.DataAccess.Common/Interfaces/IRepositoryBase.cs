﻿using RestoAPI.DataAccess.Common.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.DataAccess.Common.Interfaces
{
    public interface IRepositoryBase<TModel> where TModel: class, IEntity, new()
    {
        IQueryable<TModel> GetQueryAsNoTracking();

        Task<TModel> AddAsync(TModel model);

        Task<List<TModel>> AddRangeAsync(List<TModel> models);

        Task<TModel> UpdateAsync(TModel model);

        Task UpdateRangeAsync(List<TModel> models);

        Task DeleteAsync(TModel model);

        Task DeleteRangeAsync(List<TModel> models);

        Task DeleteByIdAsync<TKey>(TKey id)
            where TKey : IEquatable<TKey>;

        Task<TModel> GetByIdAsync<TKey>(TKey id) where TKey : IEquatable<TKey>;
    }
}
