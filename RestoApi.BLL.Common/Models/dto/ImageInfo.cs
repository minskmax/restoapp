﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Common.Models.dto
{
    public class ImageInfo
    {
        public int ImageId { get; set; }
        public int NewsId { get; set; }
        public byte[] Data { get; set; }
    }
}
