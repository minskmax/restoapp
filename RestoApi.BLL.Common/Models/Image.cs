﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Common.Models
{
    public class Image
    {
        public int? Id { get; set; }
        public byte[] Data { get; set; }
        public byte[] Thumb { get; set; }
        public int NewsId { get; set; }
    }
}
