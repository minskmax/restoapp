﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Common
{
    public class MyOptions
    {
        public MyOptions()
        {

        }

        public int MaxImageWidth { get; set; }
        public int MaxImageHeight { get; set; }
        public int ThumbWidth { get; set; }
        public int ThumHeight { get; set; }
        public int JpegQuality { get; set; }
        public string FirebaseKey { get; set; }  
        public string BonusesAdd { get; set; }
        public string BonusesSub { get; set; }
    }
}
