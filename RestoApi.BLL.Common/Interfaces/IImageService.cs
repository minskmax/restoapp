﻿using RestoApi.BLL.Common.Models;
using RestoApi.BLL.Common.Models.dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.BLL.Common.Interfaces
{
    public interface IImageService
    {
        Task<Image> AddAsync(Image image);
        Task<Image> GetAsync(int imageId);
        Task<Image> UpdateAsync(Image image);
        Task DeleteAsync(int imageId);
        Task<IEnumerable<Image>> GetForNewsAsync(int NewsId);
        Task<IEnumerable<ImageInfo>> GetForNewsThumbnails(int NewsId);
        Task<byte[]> GetThumbAsync(int imageId);
        Task<byte[]> GetImageAsync(int imageId);
        Task<int[]> GetListOfIdsAsync(int newsId); 
    }
}
