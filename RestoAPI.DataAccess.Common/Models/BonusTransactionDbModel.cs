﻿using RestoAPI.DataAccess.Common.Models;
using RestoAPI.DataAccess.Common.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.DataAccess.Common.Models
{
    public class BonusTransactionDbModel:IntEntity
    {
        public string Cause { get; set; }
        public int Count { get; set; }
        public string UserId { get; set; }
        public virtual UserDbModel User { get; set; }
    }
}
