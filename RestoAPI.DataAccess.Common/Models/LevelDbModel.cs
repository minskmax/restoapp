﻿using RestoAPI.DataAccess.Common.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.DataAccess.Common.Models
{
    public class LevelDbModel:IntEntity
    {
        public string LevelName { get; set; }
        public int BonusesNeeded { get; set; }
        public string Color { get; set; }
    }
}
