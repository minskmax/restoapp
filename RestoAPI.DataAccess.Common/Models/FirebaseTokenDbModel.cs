﻿using RestoAPI.DataAccess.Common.Models;
using RestoAPI.DataAccess.Common.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.DataAccess.Common.Models
{
    public class FirebaseTokenDbModel:IntEntity
    {        
        public string UserId { get; set; }
        public string Token { get; set; }
        public virtual UserDbModel User { get; set; }
    }
}
