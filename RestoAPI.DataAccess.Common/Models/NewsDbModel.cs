﻿using RestoAPI.DataAccess.Common.Models.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.DataAccess.Common.Models
{
    [DebuggerDisplay("News Id={Id}")]
    public class NewsDbModel:IntEntity
    {
        public string Header { get; set; }

        public string Body { get; set; }

        public List<ImageDbModel> Images { get; set; }

        public bool Push { get; set; }

        public int? AccentColor { get; set; }
        public int? BackColor { get; set; }
    }
}
