﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.DataAccess.Common.Models.Base
{
    public abstract class Entity<T>: IEntity
        where T : IEquatable<T>
    {
        public virtual T Id { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;

        public string CreatorId { get; set; }

        public DateTime Modified { get; set; } = DateTime.UtcNow;

        public string ModifierId { get; set; }
    }
}
