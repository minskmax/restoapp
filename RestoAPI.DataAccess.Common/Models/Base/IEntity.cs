﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.DataAccess.Common.Models.Base
{
    public interface IEntity
    {
        DateTime Created { get; set; }

        string CreatorId { get; set; }

        DateTime Modified { get; set; }

        string ModifierId { get; set; }
    }
}
