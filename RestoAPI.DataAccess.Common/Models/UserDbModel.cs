﻿using Microsoft.AspNetCore.Identity;
using RestoApi.DataAccess.Common.Models;
using System.Collections.Generic;

namespace RestoAPI.DataAccess.Common.Models
{
    public class UserDbModel : IdentityUser<string>
    {
        public int BonusesCount { get; set; }
        public int TotalBonusesCount { get; set; }
        public int? LevelId { get; set; }
        public virtual LevelDbModel Level { get; set; }
        public virtual ICollection<BonusTransactionDbModel> BonusTransactions {get;set;}
        public virtual UserProfileDbModel Profile { get; set; }
        public virtual FirebaseTokenDbModel FirebaseToken { get; set; }
    }
}
