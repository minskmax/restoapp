﻿using RestoAPI.DataAccess.Common.Models.Base;
using System;

namespace RestoAPI.DataAccess.Common.Models
{
    public class UserProfileDbModel : IntEntity
    {
        public string UserId { get; set; }

        public string Name { get; set; }

        public Gender Gender { get; set; }

        public DateTime BirthDate { get; set; }                        

        public string EMail { get; set; }

        public virtual UserDbModel User { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    }
}
