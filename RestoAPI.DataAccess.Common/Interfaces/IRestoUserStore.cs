﻿using Microsoft.AspNetCore.Identity;
using RestoAPI.DataAccess.Common.Models;

namespace RestoAPI.DataAccess.Common.Interfaces
{
    public interface IRestoUserStore : IUserStore<UserDbModel> { }
}
