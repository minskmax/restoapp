﻿using Microsoft.IdentityModel.Tokens;
using RestoApi.BLL.Common.Models.dto;
using RestoAPI.BLL.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.BLL.Common.Interfaces
{
    public interface IUserService
    {
        Task<string> SendCodeAsync(string phone);
        Task<User> RegisterAsync(string phone);
        Task<string> AuthenticateAsync(string phone, string code);
        Task<User> GetUserByName(string name);
        Task<User> GetUserById(string id);
        Task<bool> IsInRole(string userName, string roleName);
    }
}
