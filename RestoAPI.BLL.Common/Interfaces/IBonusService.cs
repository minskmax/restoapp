﻿using RestoApi.BLL.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.BLL.Common.Interfaces
{
    public interface IBonusService
    {
        Task<BonusTransaction> AddAsync(BonusTransaction transaction);
        Task DeleteAsync(int id);
        Task<BonusTransaction> GetAsync(int id);
        Task<IEnumerable<BonusTransaction>> GetForUserAsync(string phone, int count);
        Task<int> GetInactiveCount(string userId);
    }
}
