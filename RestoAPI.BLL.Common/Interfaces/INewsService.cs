﻿using RestoAPI.BLL.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.BLL.Common.Interfaces
{
    public interface INewsService
    {
        Task<News> AddAsync(News news);
        Task<News> GetAsync(int newsId);
        Task<News> UpdateAsync(News news);
        Task DeleteAsync(int newsId);
        Task<IEnumerable<News>> List(int offset, int limit);
        Task<int> Count();
        Task<bool> TrySetColors(int id,int accentColor, int backColor);
    }
}
