﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.BLL.Common.Interfaces
{
    public interface ISmsSender
    {
        Task<string> SendSms(string phoneNumber, string message);
    }
}
