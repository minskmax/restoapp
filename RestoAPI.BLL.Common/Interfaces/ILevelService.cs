﻿using RestoApi.BLL.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.BLL.Common.Interfaces
{
    public interface ILevelService
    {
        Task<Level> AddAsync(Level level);
        Task DeleteAsync(int id);
        Task<Level> UpdateAsync(Level level);
        Task<Level> GetAsync(int id);
    }
}
