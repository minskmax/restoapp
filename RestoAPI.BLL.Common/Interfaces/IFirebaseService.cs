﻿using RestoApi.BLL.Common.Models;
using RestoAPI.BLL.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.BLL.Common.Interfaces
{
    public interface IFirebaseService
    {
        Task<string> GetTokenForUser(User user);        
        Task<int> AddOrUpdate(string token, string userId=null);

        Task<bool> BroadcastMessage(FirebaseMessage message);
        Task<bool> BonusMessage(FirebaseMessage message);        
    }
}
