﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Common.Interfaces
{
    public interface ICurrentUserService
    {
        string CurrentUserId { get; }
    }
}
