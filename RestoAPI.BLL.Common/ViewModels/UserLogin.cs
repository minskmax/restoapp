﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.BLL.Common.Models
{
    public class UserLogin
    {
        public string Phone { get; set; }
        public string Code { get; set; }
    }
}
