﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Common.Models.dto
{
    public class FirebaseTokenUpdate
    {
        public string Token { get; set; }        
    }
}
