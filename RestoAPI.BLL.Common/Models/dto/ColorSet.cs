﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Common.Models.dto
{
    public class ColorSet
    {
        public int Id { get; set; }
        public int AccentColor { get; set; }
        public int BackColor { get; set; }
    }
}
