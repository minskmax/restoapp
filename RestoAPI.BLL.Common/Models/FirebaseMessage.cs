﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Common.Models
{
    public class FirebaseMessage
    {
        public string Header { get; set; }
        public string Body { get; set; }        
        public string Phone { get; set; }
    }
}
