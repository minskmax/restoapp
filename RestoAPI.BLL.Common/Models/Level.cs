﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Common.Models
{
    public class Level
    {
        public int Id { get; set; }
        public string LevelName { get; set; }
        public int BonusesNeeded { get; set; }
        public string Color { get; set; }
    }
}
