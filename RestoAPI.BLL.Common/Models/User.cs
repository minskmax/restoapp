﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.BLL.Common.Models
{
    public class User
    {
        public string Id { get; set; }
        public string Phone { get; set; }
        public int Bonuses { get; set; }
        public int TotalBonuses { get; set; }
        public string LevelName { get; set; }
        public string LevelColor { get; set; }
        public string FirebaseToken { get; set; }
    }
}
