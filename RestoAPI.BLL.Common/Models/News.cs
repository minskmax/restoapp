﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.BLL.Common.Models
{
    [DebuggerDisplay("News Id={Id}")]
    public class News
    {
        public int? Id { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public bool Push { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public int? AccentColor { get; set; }
        public int? BackColor { get; set; }
        public List<string> Images { get; set; } = new List<string>();
    }
}
