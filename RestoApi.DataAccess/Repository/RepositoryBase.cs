﻿using Microsoft.EntityFrameworkCore;
using RestoApi.DataAccess.Common.Interfaces;
using RestoApi.DataAccess.Context;
using RestoAPI.DataAccess.Common.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.DataAccess.Repository
{
    public class RepositoryBase<TModel> : IRepositoryBase<TModel> where TModel : class, IEntity, new()
    {
        private readonly RestoContext _context;
        private readonly DbSet<TModel> _dbSet;

        public RepositoryBase(RestoContext context)
        {
            _context = context;
            _dbSet = context.Set<TModel>();
        }

        public async Task<TModel> AddAsync(TModel model)
        {
            await _dbSet.AddAsync(model).ConfigureAwait(false);
            await _context.SaveChangesAsync().ConfigureAwait(false);
            return model;
        }

        public async Task<List<TModel>> AddRangeAsync(List<TModel> models)
        {
            await _dbSet.AddRangeAsync(models).ConfigureAwait(false);
            await _context.SaveChangesAsync().ConfigureAwait(false);
            return models;
        }

        public async Task DeleteAsync(TModel model)
        {
            _dbSet.Remove(model);
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteByIdAsync<TKey>(TKey id) where TKey : IEquatable<TKey>
        {
            var model = await _dbSet.FindAsync(id).ConfigureAwait(false);
            if (model == null)
            {
                throw new ArgumentException();
            }

            _dbSet.Remove(model);

            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteRangeAsync(List<TModel> models)
        {
            _dbSet.RemoveRange(models);
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<TModel> GetByIdAsync<TKey>(TKey id) where TKey : IEquatable<TKey>
        {
            return await _dbSet.FindAsync(id).ConfigureAwait(false);
        }

        public System.Linq.IQueryable<TModel> GetQueryAsNoTracking()
        {
            return _dbSet.AsNoTracking().AsQueryable();
        }

        public async Task<TModel> UpdateAsync(TModel model)
        {
            model.Modified = DateTime.UtcNow;
            _context.Update(model);
            await _context.SaveChangesAsync().ConfigureAwait(false);

            return model;
        }

        public async Task UpdateRangeAsync(List<TModel> models)
        {
            foreach (var model in models)
            {
                model.Modified = DateTime.UtcNow;
                _context.Update(model);
            }
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
