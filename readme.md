RestoApp backend.
Asp Net Core 2.0 project

Projects:
  Data Access - data access layer with Entity Framework
  BLL - business logic layer
  Security - JWT security tokens consumptions and issuing
  WebApi - webapi controllers