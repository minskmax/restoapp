﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Services;
using RestoApi.DataAccess.Common.Interfaces;
using RestoApi.DataAccess.Common.Models;
using RestoAPI.BLL.Common.Interfaces;
using RestoAPI.BLL.Common.Models;
using RestoAPI.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.BLL.Services
{
    public class NewsService : BaseService, INewsService
    {
        private readonly IRepositoryBase<NewsDbModel> _newsRepo;
        private readonly IRepositoryBase<ImageDbModel> _imageRepo;
        private readonly ICurrentUserService _currentUser;

        public NewsService(IRepositoryBase<NewsDbModel> repository, IMapper mapper, ICurrentUserService current, IRepositoryBase<ImageDbModel> imageRepo):base(mapper)
        {
            _newsRepo = repository;
            _imageRepo = imageRepo;
            _currentUser = current;
        }

        public async Task<News> AddAsync(News news)
        {
            var adding = _mapper.Map<NewsDbModel>(news);
            adding.CreatorId = _currentUser.CurrentUserId;
            var resultDbModel = await _newsRepo.AddAsync(adding).ConfigureAwait(false);
            news = _mapper.Map<News>(resultDbModel);
            return news;
        }

        public async Task DeleteAsync(int newsId)
        {
            await _newsRepo.DeleteByIdAsync(newsId).ConfigureAwait(false);
        }

        public Task<News> GetAsync(int newsId)
        {
            throw new NotImplementedException();
        }

        public async Task<News> UpdateAsync(News news)
        {
            var updating = _mapper.Map<NewsDbModel>(news);
            updating.ModifierId = _currentUser.CurrentUserId;
            var resultDbModel = await _newsRepo.UpdateAsync(updating).ConfigureAwait(false);
            news = _mapper.Map<News>(resultDbModel);
            return news;
        }

        public async Task<IEnumerable<News>> List(int offset=0, int limit=10)
        {
            var news = _mapper.Map<IEnumerable<News>>(await _newsRepo.GetQueryAsNoTracking().OrderByDescending(x=>x.Created).Skip(offset).Take(limit).ToListAsync().ConfigureAwait(false));
            foreach (var item in news)
            {
                var images = _imageRepo.GetQueryAsNoTracking().Where(x => x.NewsId == item.Id);
                foreach (var img in images)
                {
                    item.Images.Add(img.Id.ToString());
                }
            }
            return news;
        }

        public async Task<int> Count()
        {
            return await _newsRepo.GetQueryAsNoTracking().CountAsync();
        }

        public async Task<bool> TrySetColors(int id,int accentColor, int backColor)
        {
            var news = await _newsRepo.GetQueryAsNoTracking().Where(x => x.Id == id).SingleOrDefaultAsync().ConfigureAwait(false);
            if (news!=null)
            {
                if (news.AccentColor==null)
                {
                    news.AccentColor = accentColor;
                    news.BackColor = backColor;
                    await _newsRepo.UpdateAsync(news).ConfigureAwait(false);
                    return true;
                }
            }
            return false;
        }
    }
}
