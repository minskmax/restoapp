﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RestoApi.BLL.Common;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Common.Models;
using RestoApi.DataAccess.Common.Interfaces;
using RestoApi.DataAccess.Common.Models;
using RestoAPI.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoApi.BLL.Services
{
    public class BonusService : BaseService, IBonusService
    {
        IRepositoryBase<BonusTransactionDbModel> _bonusRepo;
        IRepositoryBase<LevelDbModel> _levelsRepo;
        UserManager<UserDbModel> _userManager;
        ICurrentUserService _current;
        IFirebaseService _firebaseService;
        private readonly MyOptions _options;


        public BonusService(IMapper mapper, ICurrentUserService current,
            UserManager<UserDbModel> userManager,
            IFirebaseService firebaseService,
            IRepositoryBase<BonusTransactionDbModel> bonusRepo,
            IRepositoryBase<LevelDbModel> levels,
            IOptions<MyOptions> options) : base(mapper)
        {
            _bonusRepo = bonusRepo;
            _levelsRepo = levels;
            _userManager = userManager;
            _current = current;
            _firebaseService = firebaseService;
            _options = options.Value;
        }

        public async Task<BonusTransaction> AddAsync(BonusTransaction transaction)
        {
            var adding = _mapper.Map<BonusTransactionDbModel>(transaction);
            var user = await _userManager.FindByNameAsync(transaction.UserPhone);
            if (user == null) return null;
            adding.User = user;
            adding.UserId = user.Id;

            adding.CreatorId = _current.CurrentUserId;
            var resultDbModel = await _bonusRepo.AddAsync(adding).ConfigureAwait(false);
            await UpdateUserCount(resultDbModel.UserId);
            transaction = _mapper.Map<BonusTransaction>(resultDbModel);
            transaction.UserPhone = user.PhoneNumber;

            if (transaction.Count > 0)
            {
                await _firebaseService.BonusMessage(new FirebaseMessage { Header = "Поступление бонусных баллов", Body = $"Вам начислено {transaction.Count} бонусов",Phone=transaction.UserPhone });
            }
            if (transaction.Count < 0)
            {
                await _firebaseService.BonusMessage(new FirebaseMessage { Header = "Списание бонусных баллов", Body = $"Списано {transaction.Count} бонусов", Phone = transaction.UserPhone });
            }

            return transaction;
        }

        public async Task DeleteAsync(int id)
        {
            var bonus = await _bonusRepo.GetQueryAsNoTracking()
            .Where(x => x.Id == id)
            .SingleOrDefaultAsync()
            .ConfigureAwait(false);

            if (bonus != null)
            {
                var userId = bonus.UserId;
                await _bonusRepo.DeleteAsync(bonus).ConfigureAwait(false);
                await UpdateUserCount(userId);
            }
        }

        public async Task<IEnumerable<BonusTransaction>> GetForUserAsync(string phone, int count)
        {            
            var bonuses = phone != null ?
                await _bonusRepo.GetQueryAsNoTracking()
                .Include(c => c.User)
                .Where(x => x.User.PhoneNumber == phone)                
                .OrderByDescending(x => x.Created)
                .Take(count)
                .ToListAsync().ConfigureAwait(false)
                : await _bonusRepo.GetQueryAsNoTracking()
                .Include(c => c.User)
                .OrderByDescending(x => x.Created)
                .Take(count)
                .ToListAsync().ConfigureAwait(false);

            var result = new List<BonusTransaction>();

            foreach (var trans in bonuses)
            {
                var adding = _mapper.Map<BonusTransaction>(trans);

                if (trans.User != null)
                {
                    adding.UserId = trans.User.Id;
                    adding.UserPhone = trans.User.PhoneNumber;
                }

                result.Add(adding);
            }

            return result;
        }

        public async Task<int> GetInactiveCount(string userId)
        {            
            return await _bonusRepo.GetQueryAsNoTracking()
            .Where(x => x.UserId==userId && x.Count>0 && DateTime.Compare(x.Created,DateTime.Now.AddHours(-24))>0)            
            .SumAsync(x=>x.Count)
            .ConfigureAwait(false);
        }

        public async Task<BonusTransaction> GetAsync(int id)
        {
            var bonus = await _bonusRepo.GetQueryAsNoTracking()
            .Where(x => x.Id == id)
            .SingleOrDefaultAsync()
            .ConfigureAwait(false);
            return _mapper.Map<BonusTransaction>(bonus);
        }

        private async Task UpdateUserCount(string userId)
        {
            var bonuses = _bonusRepo.GetQueryAsNoTracking().Where(x => x.UserId == userId).Select(y => y.Count).Sum();
            var totalbonuse = _bonusRepo.GetQueryAsNoTracking().Where(x => x.UserId == userId && x.Count > 0).Select(y => y.Count).Sum();
            var user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
            user.BonusesCount = bonuses;
            user.TotalBonusesCount = totalbonuse;
            await _userManager.UpdateAsync(user).ConfigureAwait(false);
            await UpdateUserLevel(user).ConfigureAwait(false);
        }

        private async Task UpdateUserLevel(UserDbModel user)
        {
            var level = await _levelsRepo
                .GetQueryAsNoTracking()
                .Where(x => x.BonusesNeeded >= user.BonusesCount)
                .OrderByDescending(o => o.BonusesNeeded)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);

            if (level != null)
            {
                if (level.Id != user.LevelId)
                {
                    user.Level = level;
                    user.LevelId = level.Id;
                    await _userManager.UpdateAsync(user).ConfigureAwait(false);
                }
            }
        }
    }
}
