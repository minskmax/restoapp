﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Services
{
    public abstract class BaseService
    {
        protected readonly IMapper _mapper;
        
        public BaseService(IMapper mapper)
        {
            _mapper = mapper;
        }
    }
}
