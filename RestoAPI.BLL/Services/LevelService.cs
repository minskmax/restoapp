﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Common.Models;
using RestoApi.DataAccess.Common.Interfaces;
using RestoApi.DataAccess.Common.Models;
using RestoAPI.BLL.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.BLL.Services
{
    public class LevelService : BaseService, ILevelService
    {
        private readonly IRepositoryBase<LevelDbModel> _levelRepo;
        private readonly ICurrentUserService _currentUser;

        public LevelService(IMapper mapper,IRepositoryBase<LevelDbModel> repo, ICurrentUserService current) : base(mapper)
        {
            _levelRepo = repo;
            _currentUser = current;
        }

        public async Task<Level> AddAsync(Level level)
        {
            var adding = _mapper.Map<LevelDbModel>(level);
            adding.CreatorId = _currentUser.CurrentUserId;
            var resultDbModel = await _levelRepo.AddAsync(adding).ConfigureAwait(false);
            level = _mapper.Map<Level>(resultDbModel);
            return level;
        }

        public async Task DeleteAsync(int levelId)
        {
            await _levelRepo.DeleteByIdAsync(levelId).ConfigureAwait(false);
        }

        public async Task<Level> UpdateAsync(Level level)
        {
            var updating = _mapper.Map<LevelDbModel>(level);
            updating.ModifierId = _currentUser.CurrentUserId;
            var resultDbModel = await _levelRepo.UpdateAsync(updating).ConfigureAwait(false);
            level = _mapper.Map<Level>(resultDbModel);
            return level;
        }

        public async Task<Level> GetAsync(int id)
        {
            var level = await _levelRepo.GetQueryAsNoTracking()
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
            return _mapper.Map<Level>(level);
        }
    }
}
