﻿using RestoAPI.BLL.Common.Interfaces;
using RestoAPI.BLL.Common.Models;
using RestoAPI.DataAccess.Common.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;
using AutoMapper;
using RestoApi.BLL.Services;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Collections.Generic;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.DataAccess.Common.Interfaces;
using RestoApi.DataAccess.Common.Models;
using System.Linq;

namespace RestoAPI.BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        private UserManager<UserDbModel> _userManager;
        private RoleManager<RoleDbModel> _roleManager;        
        private ISmsSender _smsSender;
        private IFirebaseService _firebase;
        private const string secret = "fiuwerfoyuworbuyabycerbfierbfierfbeufb3445iuufniu";

        public UserService(           
            UserManager<UserDbModel> userManager,
            RoleManager<RoleDbModel> roleManager,
            IMapper mapper,
            IFirebaseService firebase,
            ISmsSender smsSender) : base(mapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _smsSender = smsSender;
            _firebase = firebase;
        }

        public async Task<User> RegisterAsync(string phone)
        {
            var user = await _userManager.FindByNameAsync(phone).ConfigureAwait(false);
            if (user == null)
            {
                user = await AddUser(new User { Phone = phone }).ConfigureAwait(false);
            }
            if (user == null)
            {
                throw new InvalidOperationException($"can`t create new user:{phone}");
            }
            var resultUser = _mapper.Map<User>(user);
            var code = await SendCodeAsync(resultUser.Phone).ConfigureAwait(false);
            return resultUser;
        }

        public async Task<string> SendCodeAsync(string phone)
        {
            var dbUser = await _userManager.FindByNameAsync(phone).ConfigureAwait(false);
            if (dbUser==null)
            {
                throw new ArgumentException($"Can`t find user by phone {phone}");
            }
            var code = await _userManager.GenerateChangePhoneNumberTokenAsync(dbUser, phone).ConfigureAwait(false);
            var send_result = await _smsSender.SendSms(phone, code).ConfigureAwait(false);
            return code;
        }

        public async Task<string> AuthenticateAsync(string phone, string code)
        {
            var user = await _userManager.FindByNameAsync(phone).ConfigureAwait(false);
            if (user == null) throw new ArgumentException($"Пользователь с номером телефон {phone} не найден.");
            var smsCheck = await _userManager.VerifyChangePhoneNumberTokenAsync(user, code, phone).ConfigureAwait(false);
            if (!smsCheck) throw new ArgumentException("Проверочный код не верен.");

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };
            //roles to claims

            var roles = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
            foreach (var role in roles)
            {
                var rol = await _roleManager.FindByNameAsync(role).ConfigureAwait(false);
                if (rol != null)
                {
                    claims.Add(new Claim(ClaimTypes.Role, rol.Name));
                    var roleClaims = await _roleManager.GetClaimsAsync(rol).ConfigureAwait(false);
                    foreach (var roleClaim in roleClaims)
                    {
                        claims.Add(roleClaim);
                    }
                }
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(365),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor) as JwtSecurityToken;
            return token.RawData;
        }

        private async Task<UserDbModel> AddUser(User model)
        {
            var user = new UserDbModel
            {
                UserName = model.Phone,
                PhoneNumber = model.Phone,
                PhoneNumberConfirmed = false
            };

            RoleDbModel adminRole = null;

            if (!string.IsNullOrEmpty(user.UserName)) //TODO temporary all users are admins! remove this!!
            {
                adminRole = await _roleManager.FindByNameAsync("Admin").ConfigureAwait(false);
                if (adminRole is null)
                {
                    adminRole = new RoleDbModel
                    {
                        Name = "Admin"
                    };
                    await _roleManager.CreateAsync(adminRole).ConfigureAwait(false);
                }
            }

            var result = await _userManager.CreateAsync(user).ConfigureAwait(false);
            if (result.Succeeded)
            {
                if (adminRole != null)
                {
                    await _userManager.AddToRoleAsync(user, adminRole.Name).ConfigureAwait(false);
                }
                return user;
            }
            return null;
        }

        public async Task<User> GetUserByName(string name)
        {
            var user = await _userManager.FindByNameAsync(name);
            var result = _mapper.Map<User>(user);
            var fireToken = await _firebase.GetTokenForUser(result);
            result.FirebaseToken = fireToken;
            return result;
        }

        public async Task<User> GetUserById(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) throw new ArgumentException($"User with id {id} not found!");
            var result = _mapper.Map<User>(user);
            var fireToken = await _firebase.GetTokenForUser(result);
            result.FirebaseToken = fireToken;
            return result;
        }

        public async Task<bool> IsInRole(string userName, string roleName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            return user!=null?await _userManager.IsInRoleAsync(user, roleName):false;
        }
    }

}
