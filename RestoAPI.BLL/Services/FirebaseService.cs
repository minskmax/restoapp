﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestoApi.BLL.Common;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Common.Models;
using RestoApi.DataAccess.Common.Interfaces;
using RestoApi.DataAccess.Common.Models;
using RestoAPI.BLL.Common.Models;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.BLL.Services
{
    public class FirebaseService : IFirebaseService
    {
        private IRepositoryBase<FirebaseTokenDbModel> _repo;
        private MyOptions _options;

        public FirebaseService(IRepositoryBase<FirebaseTokenDbModel> repo, IOptions<MyOptions> options)
        {
            _repo = repo;
            _options = options.Value;
        }

        public async Task<int> AddOrUpdate(string token, string userId = null)
        {
            var exist = await _repo.GetQueryAsNoTracking().Where(x => x.Token == token).SingleOrDefaultAsync().ConfigureAwait(false);

            if (userId != null)
            {
                var existUser = await _repo.GetQueryAsNoTracking().Where(x => x.UserId == userId).SingleOrDefaultAsync().ConfigureAwait(false);
                if (existUser != null) await _repo.DeleteAsync(existUser);
            }

            if (exist != null)
            {
                exist.Token = token;
                exist.UserId = userId;
                await _repo.UpdateAsync(exist).ConfigureAwait(false);
                return exist.Id;
            }

            var resultModel = await _repo.AddAsync(new FirebaseTokenDbModel { Token = token, UserId = userId });
            return resultModel.Id;
        }

        public async Task<bool> BroadcastMessage(FirebaseMessage message)
        {
            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            //serverKey - Key from Firebase cloud messaging server  
            tRequest.Headers.Add(string.Format("Authorization: key={0}", _options.FirebaseKey));
            //Sender Id - From firebase project setting  
            //tRequest.Headers.Add(string.Format("Sender: id={0}", "XXXXX.."));
            tRequest.ContentType = "application/json";
            var payload = new
            {
                to = "/topics/news",
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = message.Body,
                    title = message.Header,
                    badge = 1
                },
                data = new
                {
                    payload = "test payload"
                }
            };

            string postbody = JsonConvert.SerializeObject(payload).ToString();
            Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = await tRequest.GetResponseAsync().ConfigureAwait(false))
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = await tReader.ReadToEndAsync().ConfigureAwait(false);
                                //result.Response = sResponseFromServer;
                                return !(string.IsNullOrEmpty(sResponseFromServer));
                            }
                    }
                }
            }
            return false;
        }

        public async Task<bool> BonusMessage(FirebaseMessage message)
        {
            var userToken = await _repo.GetQueryAsNoTracking().Include(c => c.User).Where(x => x.User.PhoneNumber == message.Phone).FirstOrDefaultAsync();

            if (userToken == null) return false;

            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            //serverKey - Key from Firebase cloud messaging server  
            tRequest.Headers.Add(string.Format("Authorization: key={0}", _options.FirebaseKey));
            //Sender Id - From firebase project setting  
            //tRequest.Headers.Add(string.Format("Sender: id={0}", "XXXXX.."));
            tRequest.ContentType = "application/json";
            var payload = new
            {
                to = userToken.Token,
                priority = "high",
                content_available = true,
                notification = new
                {
                    body = message.Body,
                    title = message.Header,
                    badge = 1
                },
                data = new
                {
                    payload = "test payload"
                }
            };

            string postbody = JsonConvert.SerializeObject(payload).ToString();
            Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = await tRequest.GetResponseAsync().ConfigureAwait(false))
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = await tReader.ReadToEndAsync().ConfigureAwait(false);
                                //result.Response = sResponseFromServer;
                                return !(string.IsNullOrEmpty(sResponseFromServer));
                            }
                    }
                }
            }
            return false;
        }


        public async Task<string> GetTokenForUser(User user)
        {
            var token = await _repo.GetQueryAsNoTracking().Where(x => x.UserId == user.Id).SingleOrDefaultAsync().ConfigureAwait(false);
            if (token != null)
            {                
                return token.Token;                
            }
            return null;
        }

    }
}
