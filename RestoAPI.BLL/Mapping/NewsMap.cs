﻿using AutoMapper;
using RestoApi.DataAccess.Common.Models;
using RestoAPI.BLL.Common.Models;
using RestoAPI.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.BLL.Mapping
{
    public class NewsMap : Profile
    {
        public NewsMap()
        {
            CreateMap<NewsDbModel, News>()
                .ForMember(m => m.Id, opt => opt.MapFrom(d => d.Id))
                .ForMember(m => m.Header, opt => opt.MapFrom(d => d.Header))
                .ForMember(m => m.Body, opt => opt.MapFrom(d => d.Body))
                .ForMember(m => m.Push, opt => opt.MapFrom(d => d.Push))
                .ForMember(m => m.Created, opt => opt.MapFrom(d => d.Created))
                .ForMember(m=>m.Modified, opt=>opt.MapFrom(d=>d.Modified))
                .ForMember(m=>m.AccentColor,opt=>opt.MapFrom(d=>d.AccentColor))
                .ForMember(m=>m.BackColor,opt=>opt.MapFrom(d=>d.BackColor))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<News, NewsDbModel>()
                .ForMember(m => m.Id, opt => opt.MapFrom(d => d.Id))
                .ForMember(m => m.Header, opt => opt.MapFrom(d => d.Header))
                .ForMember(m => m.Body, opt => opt.MapFrom(d => d.Body))
                .ForMember(m => m.Push, opt => opt.MapFrom(d => d.Push))
                .ForMember(m=>m.Created,opt=>opt.MapFrom(d=>d.Created))
                .ForMember(m => m.Modified, opt => opt.MapFrom(d => d.Modified))
                .ForMember(m => m.AccentColor, opt => opt.MapFrom(d => d.AccentColor))
                .ForMember(m => m.BackColor, opt => opt.MapFrom(d => d.BackColor))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
