﻿using AutoMapper;
using RestoAPI.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAPI.BLL.Mapping.User
{
    public class UserMap : Profile
    {
        public UserMap()
        {
            CreateMap<RestoAPI.BLL.Common.Models.User, UserDbModel>()
                .ForMember(m => m.Id, opt => opt.Ignore())
                .ForMember(m => m.Email, opt => opt.Ignore())
                .ForMember(m => m.UserName, opt => opt.MapFrom(s => s.Phone))     
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<UserDbModel, RestoAPI.BLL.Common.Models.User>()
                .ForMember(m => m.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(m => m.Phone, opt => opt.MapFrom(s => s.UserName))
                .ForMember(m=>m.Bonuses, opt => opt.MapFrom(s=>s.BonusesCount))
                .ForMember(m=>m.TotalBonuses,opt=>opt.MapFrom(s=>s.TotalBonusesCount))
                .ForMember(m=>m.LevelName,opt=>opt.MapFrom(s=>s.Level.LevelName))
                .ForMember(m=>m.LevelColor,opt=>opt.MapFrom(s=>s.Level.Color))
                .ForAllOtherMembers(opt => opt.Ignore());


            CreateMap<RestoAPI.BLL.Common.Models.User, RestoAPI.BLL.Common.Models.UserLogin>()
                .ForMember(m => m.Phone, opt => opt.MapFrom(s => s.Phone))
                .ReverseMap()
                .ForAllOtherMembers(opt => opt.Ignore());
                

        }
    }
}
