﻿using AutoMapper;
using RestoApi.BLL.Common.Models;
using RestoApi.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Mapping
{
    public class LevelMap:Profile
    {
        public LevelMap()
        {
            CreateMap<LevelDbModel, Level>()
                .ForMember(c=>c.Id,opt=>opt.MapFrom(d=>d.Id))
                .ForMember(c => c.BonusesNeeded, opt => opt.MapFrom(d => d.BonusesNeeded))
                .ForMember(c => c.Color, opt => opt.MapFrom(d => d.Color))
                .ForMember(c => c.LevelName, opt => opt.MapFrom(d => d.LevelName))
                .ReverseMap()
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
