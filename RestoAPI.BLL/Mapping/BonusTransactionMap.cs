﻿using AutoMapper;
using RestoApi.BLL.Common.Models;
using RestoApi.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Mapping
{
    public class BonusTransactionMap : Profile
    {
        public BonusTransactionMap()
        {
            CreateMap<BonusTransactionDbModel, BonusTransaction>()
                .ForMember(c => c.Id, opt => opt.MapFrom(d => d.Id))
                .ForMember(c => c.Cause, opt => opt.MapFrom(d => d.Cause))
                .ForMember(c => c.Count, opt => opt.MapFrom(d => d.Count))
                .ForMember(c => c.UserId, opt => opt.MapFrom(d => d.UserId))                
                .ForMember(c=>c.CreatorId, opt => opt.MapFrom(d=>d.CreatorId))
                .ForMember(c=>c.Date, opt => opt.MapFrom(d=>d.Created))
                .ReverseMap()
                .ForMember(c=>c.Created, opt => opt.Ignore())
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
