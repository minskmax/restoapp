﻿using RestoAPI.BLL.Common.Interfaces;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace RestoAPI.BLL.Twilio
{
    public class TwilioSmsSender : ISmsSender
    {
        private readonly string _twilioID;
        private readonly string _twilioToken;

        public TwilioSmsSender()
        {
            _twilioID = "AC6f91591f1b2d5813ac4387114c0c9326";
            _twilioToken = "0f381378aeb0db906c2da3f8f9cc54a6";
        }
        public async Task<string> SendSms(string phoneNumber, string message)
        {
            TwilioClient.Init(_twilioID, _twilioToken);

            var mess = await MessageResource.CreateAsync(
                body: message,
                from: new PhoneNumber("+18034559893"),
                to: new PhoneNumber(phoneNumber)
            ).ConfigureAwait(false);
            return mess.Sid;
        }
    }
}
