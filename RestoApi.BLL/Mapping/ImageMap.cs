﻿using AutoMapper;
using RestoApi.BLL.Common.Models;
using RestoApi.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoApi.BLL.Mapping
{
    public class ImageMap : Profile
    {
        public ImageMap()
        {
            CreateMap<ImageDbModel, Image>()
                    .ForMember(m => m.Id, opt => opt.MapFrom(d => d.Id))
                    .ForMember(m => m.Data, opt => opt.MapFrom(d => d.Image))
                    .ForMember(m => m.NewsId, opt => opt.MapFrom(d => d.NewsId))
                    .ReverseMap()
                    .ForMember(m => m.Thumbnail, opt => opt.MapFrom(d => d.Thumb))
                    .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
