﻿using AutoMapper;
using RestoApi.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using RestoApi.BLL.Common.Models.dto;
using RestoAPI.BLL.Common.Models;

namespace RestoApi.BLL.Mapping.dto
{
    public class DtoMap:Profile
    {
        public DtoMap()
        {
            CreateMap<ImageDbModel, ImageInfo>()
                    .ForMember(m => m.ImageId, opt => opt.MapFrom(d => d.Id))
                    .ForMember(m => m.NewsId, opt => opt.MapFrom(d => d.NewsId))
                    .ForAllOtherMembers(opt => opt.Ignore());            
        }
    }
}
