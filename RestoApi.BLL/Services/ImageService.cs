﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using PhotoSauce.MagicScaler;
using RestoApi.BLL.Common;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Common.Models;
using RestoApi.BLL.Common.Models.dto;
using RestoApi.DataAccess.Common.Interfaces;
using RestoApi.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoApi.BLL.Services
{
    public class ImageService : BaseService, IImageService
    {
        private readonly IRepositoryBase<NewsDbModel> _newsRepo;
        private readonly IRepositoryBase<ImageDbModel> _imagesRepo;
        private readonly MyOptions _options;

        public ImageService(IMapper mapper, 
            IRepositoryBase<NewsDbModel> newsRepo,
            IRepositoryBase<ImageDbModel> imagesRepo,
            IOptions<MyOptions> options) :base(mapper)
        {
            _newsRepo = newsRepo;
            _imagesRepo = imagesRepo;
            _options = options.Value;
        }

        public async Task<Image> AddAsync(Image image)
        {
            var img = _mapper.Map<ImageDbModel>(image);

            img.Image = ScaleImage(img.Image, false);
            img.Thumbnail = ScaleImage(img.Image, true);

            var result = await _imagesRepo.AddAsync(img).ConfigureAwait(false);
            var news = await _newsRepo.GetQueryAsNoTracking().Where(x => x.Id == result.NewsId).SingleOrDefaultAsync().ConfigureAwait(false);
            await _newsRepo.UpdateAsync(news);

            return _mapper.Map<Image>(result);
        }

        public async Task DeleteAsync(int imageId)
        {
            var image = await _imagesRepo.GetQueryAsNoTracking().Where(x => x.Id == imageId).Include(x=>x.News).SingleOrDefaultAsync().ConfigureAwait(false);
            var parent = image.News;
            await _newsRepo.UpdateAsync(parent);
            await _imagesRepo.DeleteAsync(image).ConfigureAwait(false);            
        }

        public async Task<Image> GetAsync(int imageId)
        {
            var img = await _imagesRepo.GetQueryAsNoTracking()
                .Where(x=>x.Id==imageId)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
            return _mapper.Map<Image>(img);
        }

        public async Task<byte[]> GetThumbAsync(int imageId)
        {
            return await _imagesRepo.GetQueryAsNoTracking()
                .Where(x => x.Id == imageId)
                .Select(x=>x.Thumbnail)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task<byte[]> GetImageAsync(int imageId)
        {
            return await _imagesRepo.GetQueryAsNoTracking().Where(x => x.Id == imageId)
                .Select(x => x.Image)
                .SingleOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async Task<IEnumerable<Image>> GetForNewsAsync(int newsId)
        {
            var images = await _imagesRepo.GetQueryAsNoTracking()
                .Where(x => x.NewsId == newsId)
                .OrderBy(x=>x.Created)
                .ToListAsync().ConfigureAwait(false);
            return _mapper.Map<List<Image>>(images);
        }

        public async Task<IEnumerable<ImageInfo>> GetForNewsThumbnails(int newsId)
        {
            var thumbs = await _imagesRepo.GetQueryAsNoTracking()
                .Where(x => x.NewsId == newsId)
                .OrderBy(x => x.Created)
                .Select(x => new { x.Thumbnail,x.Id,x.NewsId})
                .ToListAsync().ConfigureAwait(false);

            var list = new List<ImageInfo>();

            foreach (var image in thumbs)
            {
                list.Add(new ImageInfo
                {
                    ImageId = image.Id,
                    NewsId = image.NewsId,
                    Data = image.Thumbnail
                }
                    );
            }
            return list;
        }

        public async Task<Image> UpdateAsync(Image image)
        {
            var imageDb = _mapper.Map<ImageDbModel>(image);
            return _mapper.Map<Image>(await _imagesRepo.UpdateAsync(imageDb).ConfigureAwait(false));
        }

        private byte[] ScaleImage(byte[] input, bool thumb)
        {
            var settings = new ProcessImageSettings
            {
                Width = _options.MaxImageWidth,
                Height = _options.MaxImageHeight,
                ResizeMode = CropScaleMode.Max,
                SaveFormat = FileFormat.Jpeg,
                JpegQuality = _options.JpegQuality,
                JpegSubsampleMode = ChromaSubsampleMode.Subsample420
            };

            if (thumb)
            {
                settings.Width = _options.ThumbWidth;
                settings.Height = _options.ThumHeight;
                settings.ResizeMode = CropScaleMode.Crop;
            }

            byte[] output;
            using (var stream = new MemoryStream())
            {
                MagicImageProcessor.ProcessImage(input, stream, settings);
                output = stream.ToArray();
            }
            return output;
        }

        public async Task<int[]> GetListOfIdsAsync(int newsId)
        {
            var ids = await _imagesRepo.GetQueryAsNoTracking()
                .Where(x => x.NewsId == newsId)
                .Select(x=>x.Id)
                .ToArrayAsync().ConfigureAwait(false);

            return ids;
        }
    }
}
