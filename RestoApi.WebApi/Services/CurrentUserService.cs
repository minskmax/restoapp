﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using RestoApi.BLL.Common.Interfaces;
using RestoAPI.DataAccess.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoApi.WebApi.Services
{
    public class CurrentUserService:ICurrentUserService
    {
        IHttpContextAccessor _context;
        UserManager<UserDbModel> _userManager;


        public CurrentUserService(IHttpContextAccessor context, UserManager<UserDbModel> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public string CurrentUserId
        {
            get
            {
                return _userManager.GetUserId(_context.HttpContext.User);
            }
        }
    }
}
