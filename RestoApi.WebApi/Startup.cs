﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Services;
using RestoApi.DataAccess.Common.Interfaces;
using RestoApi.DataAccess.Context;
using RestoApi.DataAccess.Repository;
using RestoApi.WebApi.ExamplesProviders;
using RestoApi.WebApi.Services;
using RestoAPI.BLL.Common.Interfaces;
using RestoAPI.BLL.Mapping.User;
using RestoAPI.BLL.Services;
using RestoAPI.BLL.Twilio;
using RestoAPI.DataAccess.Common.Models;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.Swagger;

namespace RestoApi.WebApi
{
    public class Startup
    {
        private const string secret = "fiuwerfoyuworbuyabycerbfierbfierfbeufb3445iuufniu";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddAutoMapper(Assembly.GetAssembly(typeof(UserMap)));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<RestoContext>(options => options.UseSqlServer
            (Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<UserDbModel, RoleDbModel>(
                o =>
                {
                    o.Tokens.ChangePhoneNumberTokenProvider = "Phone";
                    })
                .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<RestoContext>();

            services.Configure<IdentityOptions>(options =>
            {
                options.User.RequireUniqueEmail = false;
                options.User.AllowedUserNameCharacters = "0123456789+";
            });

            //Auth
            var key = Encoding.ASCII.GetBytes(secret);

            services.AddAuthentication(options=>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };

                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "RestoApi"
                });

                c.ExampleFilters();                
                c.OperationFilter<AddResponseHeadersFilter>(); // [SwaggerResponseHeader]                

                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });

                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>()
                {
                 { "Bearer", new string[]{ } }
                });
            });

            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<ISmsSender, TwilioSmsSender>();
            services.AddScoped<IImageService, ImageService>();
            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<IFirebaseService, FirebaseService>();
            services.AddScoped<IBonusService, BonusService>();
            services.AddSwaggerExamplesFromAssemblyOf<TokenRequestExample>();

            services.Configure<BLL.Common.MyOptions>(options => Configuration.GetSection("MySettings").Bind(options));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {                
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseAuthentication();

            app.UseMvc();

            app.UseStaticFiles();
        }
    }
}
