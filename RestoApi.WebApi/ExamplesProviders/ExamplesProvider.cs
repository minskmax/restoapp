﻿using RestoApi.BLL.Common.Models.dto;
using RestoAPI.BLL.Common.Models;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoApi.WebApi.ExamplesProviders
{
    public class TokenRequestExample : IExamplesProvider<TokenRequest>
    {
        public TokenRequest GetExamples()
        {
            return new TokenRequest
            {
                Phone = "+79630879782",
                Code = "9999"
            };
        }
    }

    public class UserLoginExample : IExamplesProvider<UserLogin>
    {
        public UserLogin GetExamples()
        {
            return new UserLogin
            {
                Phone = "+79630879782",
                Code = "9999"
            };
        }
    }
}
