﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Common.Models.dto;
using RestoAPI.BLL.Common.Interfaces;
using RestoAPI.BLL.Common.Models;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RestoAPI.Controllers
{
    [Route("api/news")]
    public class NewsController:ControllerBase
    {
        private INewsService _service;
        private IImageService _imagesService;

        public NewsController(INewsService service, IImageService imageService)
        {
            _service = service;
            _imagesService = imageService;
        }

        [HttpPost,Authorize(Roles = "Admin")]
        [Route("")]
        public async Task<IActionResult> CreateNews([FromBody]News model)
        {
            var result = await _service.AddAsync(model);
            return Ok(result);
        }

        [HttpPost,Authorize(Roles = "Admin")]
        [Route("update")]
        public async Task<IActionResult> UpdateNews([FromBody]News model)
        {
            var result = await _service.UpdateAsync(model);
            return Ok(result);
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("delete")]
        public async Task<IActionResult> DeleteNews([FromBody]int id)
        {
            try
            {
                await _service.DeleteAsync(id);
            }
            catch (ArgumentException)
            {

                return BadRequest();
            }
            return Ok();
        }

        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> GetNewsAsync(int start, int limit)
        {
            if (limit < 1) limit = 1;
            if (start < 0) start = 0;
            var news = await _service.List(start, limit);
            foreach (var item in news)
            {
                for (int i = 0; i < item.Images.Count; i++)
                {
                    item.Images[i] = $"{this.Request.Scheme}://{this.Request.Host}/api/images/getFile?imageId={item.Images[i]}";
                }
            }
            return Ok(news);
        }

        [HttpGet]
        [Route("count")]
        public async Task<int> Count()
        {
            return await _service.Count();
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("setcolors")]
        public async Task<IActionResult> TrySetColors([FromBody]ColorSet colors)
        {
            var result = await _service.TrySetColors(colors.Id, colors.AccentColor, colors.BackColor);
            return Ok(result);
        }
    }
}