﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using RestoApi.BLL.Common;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Common.Models;
using RestoApi.BLL.Common.Models.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoApi.WebApi.Controllers
{
    [Route("api/messages")]
    public class MessagingController:ControllerBase
    {
        private IFirebaseService _service;
        private ICurrentUserService _current;

        public MessagingController(IFirebaseService service, ICurrentUserService current)
        {
            _service = service;
            _current = current;
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("broadcast")]
        public async Task<IActionResult> MessageBroadcast([FromBody]FirebaseMessage message)
        {
            if (await _service.BroadcastMessage(message))
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("bonus_message")]
        public async Task<IActionResult> MessageBonus([FromBody]FirebaseMessage message)
        {
            if (await _service.BonusMessage(message))
            {
                return Ok();
            }
            return BadRequest();
        }


        [HttpPost, AllowAnonymous]
        [Route("token")]
        public async Task<IActionResult> UpdateToken([FromBody]FirebaseTokenUpdate token)
        {
            var currentUserId = _current.CurrentUserId;            
            var id = await _service.AddOrUpdate(token.Token, currentUserId);
            return Ok(id);
        }
    }
}
