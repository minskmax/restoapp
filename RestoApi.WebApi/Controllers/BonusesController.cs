﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Common.Models;
using RestoApi.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoApi.WebApi.Controllers
{
    [Route("api/bonuses")]
    public class BonusesController:ControllerBase
    {
        private IBonusService _service;

        public BonusesController(IBonusService service)
        {
            _service = service;
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("")]
        public async Task<IActionResult> AddTransaction([FromBody]BonusTransaction model)
        {
            var result = await _service.AddAsync(model);
            return Ok(result);
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("delete")]
        public async Task<IActionResult> RemoveTransaction([FromBody] int id)
        {
            await _service.DeleteAsync(id);
            return Ok();
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Route("list")]
        public async Task<IActionResult> GetTransactions(string phone, int count)
        {
            if (phone == string.Empty) phone = null;
            return Ok(await _service.GetForUserAsync(phone,count));
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Route("inactive")]
        public async Task<IActionResult> GetInactiveBonuses(string userId)
        {
            if (userId == null) return BadRequest("Phone is null error");            
            return Ok(await _service.GetInactiveCount(userId));
        }
    }
}
