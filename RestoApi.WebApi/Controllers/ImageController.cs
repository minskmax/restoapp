﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RestoApi.BLL.Common.Interfaces;
using RestoApi.BLL.Common.Models;
using RestoApi.BLL.Common.Models.dto;
using RestoApi.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoApi.WebApi.Controllers
{
    [Route("api/images")]
    public class ImageController : ControllerBase
    {
        private IImageService _service;

        public ImageController(IImageService _imageService)
        {
            _service = _imageService;
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("")]
        public async Task<IActionResult> CreateImage([FromBody]Image model)
        {
            var result = await _service.AddAsync(model);
            return Ok(result.Id);
        }

        [HttpPost, Authorize(Roles = "Admin")]
        [Route("delete")]
        public async Task<IActionResult> DeleteImage([FromBody]int id)
        {
            try
            {
                await _service.DeleteAsync(id);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> GetList(int newsId)
        {
            var images = await _service.GetForNewsAsync(newsId);            
            return Ok(images);
        }

        [HttpGet]
        [Route("get")]
        public async Task<IActionResult> GetImage(int imageId)
        {
            var bytes = await _service.GetImageAsync(imageId);
            if (bytes != null)
            {
                var image =
                    new ImageInfo
                    {
                        Data = bytes,
                        NewsId = -1,
                        ImageId = imageId
                    };
                return Ok(image);
            }
            return BadRequest();
        }

        
        [HttpGet]
        [Route("getFile")]
        public async Task<IActionResult> GetImageFile(int imageId)
        {
            var bytes = await _service.GetImageAsync(imageId);
            if (bytes != null)
            {
                var image =
                    new ImageInfo
                    {
                        Data = bytes,
                        NewsId = -1,
                        ImageId = imageId
                    };
                return File(image.Data, "image/jpeg");
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("getThumb")]
        public async Task<IActionResult> GetThumb(int imageId)
        {
            var bytes = await _service.GetThumbAsync(imageId);
            if (bytes != null)
            {
                var image =
                    new ImageInfo
                    {
                        Data = bytes,
                        NewsId = -1,
                        ImageId = imageId
                    };
                return Ok(image);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("listThumbs")]
        public async Task<IActionResult> GetThumbs(int newsId)
        {
            var images = await _service.GetForNewsThumbnails(newsId);
            return Ok(images.ToList());
        }

        [HttpGet]
        [Route("listIds")]
        public async Task<IActionResult> GetIds(int newsId)
        {
            var images = await _service.GetListOfIdsAsync(newsId);
            var ids = new List<ImageInfo>();
            foreach (var item in images)
            {
                ids.Add(new ImageInfo { ImageId = item, NewsId = newsId });
            }
            return Ok(ids);
        }
    }
}
