﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using RestoApi.BLL.Common;
using RestoAPI.BLL.Common.Interfaces;
using RestoAPI.BLL.Common.Models;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RestoAPI.WebApi.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService _service;
        private MyOptions _options;

        public UserController(IUserService service, IOptions<MyOptions> options)
        {
            _service = service;
            _options = options.Value;
        }

        [HttpPost]
        [Route("auth")]
        [AllowAnonymous]
        [SwaggerResponse(200,"Token string",typeof(string))]        
        [SwaggerResponse(400,"Bad request")]
        public async Task<IActionResult> AuthenticateAsync([FromBody] UserLogin login)
        {
            try
            {
                var token = await _service.AuthenticateAsync(login.Phone, login.Code);
                if (token!=null) return Ok(token);
                return BadRequest("Неизвестная ошибка авторизации");
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
         public async Task<IActionResult> RegisterUserAsync(string phone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var res = await _service.RegisterAsync(phone);

            if (res != null)
            {
                return Created("https://restoapp.ru/api/userinfo", res);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("currentUser")]
        [Authorize]
        public async Task<IActionResult> CurrentUser()
        {
            var curUser = User.Identity.Name;

            if (!string.IsNullOrEmpty(curUser))
            {
                var user = await _service.GetUserByName(curUser);
                return Ok(user);
            }

            return BadRequest("can`t get current user");
        }

        [HttpGet, Authorize]
        [Route("test")]
        public IActionResult Test(string test)
        {
            return Ok(test);
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Route("testRole")]
        public async Task<IActionResult> TestRole(string test)
        {
            if (!(await CheckAdminRole())) return Unauthorized();//Check what user still in Admin role, call in client bewore open page with admin functions
            return Ok(test);
        }

        [HttpGet, Authorize]
        [Route("byId")]
        public async Task<IActionResult> ById(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var userId = await _service.GetUserById(id);
                return Ok(userId);
            }
            return BadRequest("empty id");
        }

        private async Task<bool> CheckAdminRole()
        {
            return await _service.IsInRole(User.Identity.Name,"Admin");
        }
    }
}
